import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from './../../../environments/environment';

const endpoint = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  constructor(private http: HttpClient) { }

  //Accounts
  getAccounts(): Observable<any>{
    return this.http.get(endpoint + '/accounts');
  }

}
