import { AccountsService } from './../../services/api/accounts.service';
import { Component, OnInit } from '@angular/core';
import { Account } from 'src/app/interfaces/accounts';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-view-accounts',
  templateUrl: './view-accounts.component.html',
  styleUrls: ['./view-accounts.component.css']
})
export class ViewAccountsComponent implements OnInit {

  public accounts;
  public balance: number = 0;
  public accountInformation: Account;
  public message_response;
  public message: boolean = false;
  public error_message: boolean = false;
  public success_message: boolean = false;
  public proccessingPayment: boolean = false;
  public successfully_withdrew: boolean = false;
  public amountWithdraw;

  withDrawForm = new FormGroup({
    amount: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.pattern('[0-9]*')
    ])
  });

  constructor(public api: AccountsService) { }

  ngOnInit(): void {

    this.api.getAccounts().subscribe((data: {}) => {
      console.log(data);
      this.accounts = data;

      this.accounts.forEach( newObject => {
        var newTotal:number = +newObject.balance;

        this.balance = newTotal + this.balance;
      });

    });
  }


  withdraw(accountNumber, accountType, accountBalance){

    this.withDrawForm.reset();
    this.error_message = false;
    this.success_message = false;

    this.proccessingPayment = false;
      this.successfully_withdrew = false;

    this.accountInformation = {
      account_number: accountNumber,
      account_type: accountType,
      balance: accountBalance
    }

  }


  enteredAmount(amount){
    console.log(amount+" "+this.accountInformation.balance);

    let enteredNewAmount: number = +amount;
    let newBalance: number = +this.accountInformation.balance;

    if(enteredNewAmount > newBalance){
      this.error_message = true;
    }
    else{
      this.error_message = false;
      this.amountWithdraw = amount;
    }

  }


  getAmountDrawn(){
    // this.amountWithdraw;

    this.proccessingPayment = true;

    setTimeout(()=>{ 
      
      this.proccessingPayment = false;
      this.successfully_withdrew = true;
      
    }, 4000);

  }

}
